package com.arcane.tixer

import android.animation.TimeAnimator
import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import java.util.*

private const val KEY_UPDATE_FREQUENCY = "update_frequency"

class Clock(context: Context, container: ViewGroup?) {

    private var updateFrequencyMs = 15_000L
    private var deltas = 0L
    private val animator = TimeAnimator()
    val view = LayoutInflater.from(context).inflate(R.layout.clock_fragment, container, false)!!
    private val findLed = { id: Int -> view.findViewById<Led>(id)!! }
    private var groupA = listOf(R.id.a0, R.id.a1, R.id.a2).map(findLed)
    private var groupB = listOf(
            R.id.b0, R.id.b1, R.id.b2, R.id.b3, R.id.b4, R.id.b5,
            R.id.b6, R.id.b7, R.id.b8
    ).map(findLed)
    private var groupC = listOf(
            R.id.c0, R.id.c1, R.id.c2, R.id.c3, R.id.c4, R.id.c5
    ).map(findLed)
    private var groupD = listOf(
            R.id.d0, R.id.d1, R.id.d2, R.id.d3, R.id.d4, R.id.d5,
            R.id.d6, R.id.d7, R.id.d8
    ).map(findLed)
    private var leds = listOf(groupA, groupB, groupC, groupD).flatten()

    init {

        animator.setTimeListener { _, totalTime, deltaTime ->
            deltas += deltaTime
            if (deltas > 0) {
                refreshClock()
                deltas -= updateFrequencyMs
            }
        }

        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        val updateFrequencyMsString = pref.getString(KEY_UPDATE_FREQUENCY, "") ?: ""
        updateFrequencyMs = updateFrequencyMsString.toLong()
        Log.i(TAG, "update frequency = $updateFrequencyMs")
        animator.start()
    }

    private fun refreshClock() {
        val calendar = Calendar.getInstance()!!

        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val hour_tens = hour / 10
        val hour_ones = hour % 10
        val minutes = calendar.get(Calendar.MINUTE)
        val minute_tens = minutes / 10
        val minute_ones = minutes % 10

        groupA.setDisplayNumber(hour_tens)
        groupB.setDisplayNumber(hour_ones)
        groupC.setDisplayNumber(minute_tens)
        groupD.setDisplayNumber(minute_ones)

        leds.forEach { it.invalidate() }
    }

    private fun List<Led>.setDisplayNumber(displayNumber: Int) {
        val shuffled = this.shuffled()
        for (i in shuffled.indices) {
            val led = shuffled[i]
            led.lit = i < displayNumber
        }
    }

    companion object {
        private const val TAG = "Clock"
    }
}
