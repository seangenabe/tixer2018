package com.arcane.tixer

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View

class Led(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
        View(context, attrs, defStyleAttr) {

    private var rect: RectF? = null
    private var paint = Paint()
    private var sideLengthPixels = 0f

    var litColor = Color.MAGENTA
    var dimColor = Color.TRANSPARENT
    var lit = true

    constructor(context: Context) : this(context, null, 0)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.Led
                , 0,
                0
        )
                .apply {
                    try {
                        litColor = getColor(R.styleable.Led_litColor, Color.MAGENTA)
                    } finally {
                        recycle()
                    }
                }
    }

    override fun onAttachedToWindow() {
        if (parent == null) return
        val pixels = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                SIDE_LENGTH_DIP,
                resources!!.displayMetrics!!
        )

        val params = layoutParams
        params.height = pixels.toInt()
        params.width = pixels.toInt()

        val t = 0.1f * pixels
        val b = 0.9f * pixels

        rect = RectF(t, t, b, b)
        sideLengthPixels = pixels
        layoutParams = params

        super.onAttachedToWindow()
    }

    override fun onDraw(canvas: Canvas) {
        paint.color = if (lit) litColor else dimColor
        canvas.drawRoundRect(
                rect,
                sideLengthPixels * 0.3f,
                sideLengthPixels * 0.3f,
                paint
        )
    }

    companion object {
        private const val SIDE_LENGTH_DIP = 40f
    }
}
