package com.arcane.tixer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        if (savedInstanceState == null) {
        supportFragmentManager
                .beginTransaction()
                .add(R.id.container, SettingsFragment())
                .commit()
        }
    }
}
