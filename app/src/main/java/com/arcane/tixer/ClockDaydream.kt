package com.arcane.tixer

import android.service.dreams.DreamService

class ClockDaydream : DreamService() {

    init {
        isInteractive = false
        isFullscreen = true
        isScreenBright = true

    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setContentView(Clock(this, null).view)
    }

}